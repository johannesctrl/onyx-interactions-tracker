
# Onyx Interaction Tracker

This project creates a JavaScript script which can be embedded into an ONYX test. It does the following:
* analyzing the html of the test and creating instances of classes of tasks and task elements
* creating visual codes: qr codes for every task and ArUCo markers for every task element
* adding event listeners to input elements

The visual codes are being created for the project [eye-focus-task-tracker](https://gitlab.com/johannesctrl/eye-focus-task-tracker) which is an application using eye tracking glasses to analyse if a student is looking at a task or an element of a task. The onyx-interaction-tracker is obligated to append on these tasks before the eye-focus-task-tracker can be used. The eye-focus-task-tracker sends the results to a learning record store via a las2peer service.

The onyx-interaction-tracker also adds event listeners to input elements. All inputs done by the student will be sent to the [las2peer-eye-focus-task-tracker](https://gitlab.com/johannesctrl/las2peer-eyetrackingproxyservice) which creates an xAPI statement to send and save it to a learning record store.

The following image showes an ONYX task where the visual codes have been added.
![](show.png)

## Built
The project is written in JavaScript and uses node.js for an easier development process. With webpack the project gets built as a single JavaScript script to use it for ONYX.
```
npm run build:dev
```
or
```
npm run build:prod
```
