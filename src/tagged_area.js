const qrjs2 = require("./qrjs2")

export class Tagged_Area {
    constructor(div_item_container, number, id) {
        this.id = id
        this.number = number
        this.div_item_container = div_item_container

        this.tagged_area = document.createElement("div")
        this.left = document.createElement("div")
        this.qr_code_left = document.createElement("div")
        this.middle = document.createElement("div")
        this.right = document.createElement("div")
        this.first_right = document.createElement("div")

        this.build_structure()

    }
    build_structure() {
        // Structure
        this.tagged_area.className = "tagged_area"
        this.tagged_area.style.display = "flex"
        const highest_div = this.div_item_container.parentElement  // Dies ist der Hauptcontainer einer Aufgabe mit eigener ID, die wir aber nicht nutzen
        highest_div.appendChild(this.tagged_area)

        this.left.className = "left"
        this.left.style.width = "var(--qr-width)"
        // div_left.style.width = "200px"
        this.tagged_area.appendChild(this.left)

        this.qr_code_left.id = "qr_code_left".concat(String(this.number))
        this.left.appendChild(this.qr_code_left)

        this.middle.className = "middle"
        this.middle.style.width = "100%"
        // div_middle.style.overflowX = "auto" // in QR-Codes
        this.tagged_area.appendChild(this.middle)
        this.middle.appendChild(this.div_item_container)

        this.right.className = "right"
        this.right.style.width = "var(--qr-width)"
        // div_right.style.width = "200px"
        this.right.style.display = "flex"
        this.right.style.flexDirection = "column"
        this.tagged_area.appendChild(this.right)

        this.first_right.style.display = "flex"
        this.first_right.style.flex = "1"
        this.right.appendChild(this.first_right)

        this.qr_code_right = document.createElement("div")
        this.qr_code_right.id = "qr_code_right".concat(String(this.number))
        this.right.appendChild(this.qr_code_right)

        // QR-Codes Creation
        const id_top = this.id + " top"
        const id_bot = this.id + " bot"
        var qr_code_top = document.createElement("img"),
        sTop = qrjs2.QRCode.generatePNG(id_top, {
            //options here
            ecclevel: "Q",
            format: "html",
            margin: 0,
            modulesize: 8
        });
        var qr_code_bod = document.createElement("img"),
        sBot = qrjs2.QRCode.generatePNG(id_bot, {
            //options here
            ecclevel: "Q",
            format: "html",
            margin: 0,
            modulesize: 8
        });
        qr_code_top.src = sTop
        qr_code_bod.src = sBot
        this.qr_code_left.appendChild(qr_code_top)
        this.qr_code_right.appendChild(qr_code_bod)

        //CSS Styles
        this.qr_code_left.style.position = "sticky"
        this.qr_code_left.style.top = "10px"
        this.qr_code_left.style.margin = "10px"
        this.qr_code_right.style.position = "sticky"
        this.qr_code_right.style.marginTop = "auto"
        this.qr_code_right.style.marginLeft = "10px"
        this.qr_code_right.style.marginBottom = "10px"
        this.qr_code_right.style.marginRight = "10px"
        this.qr_code_right.style.paddingTop = "10px" // padding weil marginTop auf "auto" muss
        this.qr_code_right.style.bottom = "10px"
        const qr_code_left_img = this.qr_code_left.getElementsByTagName("img")[0]
        qr_code_left_img.style.width = "160px"
        qr_code_left_img.style.height = "100%"
        const qr_code_right_img = this.qr_code_right.getElementsByTagName("img")[0]
        qr_code_right_img.style.width = "160px"
        qr_code_right_img.style.height = "100%"
    }
}