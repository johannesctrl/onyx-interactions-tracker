import { adaptive_marking_element, inline_marking_element } from "./aruco"


export class Task_Element {
    constructor(element, name, task) {
        this.element = element
        this.name = name
        this.task = task

        this.id = this.create_id()
        this.set_attributes()
        this.create_arucos()
    }
    create_id() {
        if(this.name == "explaination-section") {
            return this.task.task_element_numbering_scheme.explaination_section.get_number()
        }
        if(this.name == "interaction-section") {
            return this.task.task_element_numbering_scheme.interaction_section.get_number()
        }
        if(this.name == "textfield") {
            return this.task.task_element_numbering_scheme.textfield.get_number()
        }
        if(this.name == "hottext") {
            return this.task.task_element_numbering_scheme.textfield.get_number()
        }
        if(this.name == "figure") {
            return this.task.task_element_numbering_scheme.figure.get_number()
        }
    }
    set_attributes() {
        this.element.setAttribute("task-element", "true")
        this.element.setAttribute("class", this.name)
        this.element.setAttribute("task-element-id", this.id)
    }
    create_arucos() {
        if(this.name == "explaination-section") {
            adaptive_marking_element(this.task.number, this.element, this.id)
        }
        if(this.name == "interaction-section") {
            adaptive_marking_element(this.task.number, this.element, this.id)
        }
        if(this.name == "figure") {
            adaptive_marking_element(this.task.number, this.element, this.id)
        }
        if(this.name == "textfield") {
            inline_marking_element(this.element, this.id)
        }
        if(this.name == "hottext") {
            inline_marking_element(this.element, this.id)
        }
    }
}