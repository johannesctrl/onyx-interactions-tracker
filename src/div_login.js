export class Div_Login {
    constructor() {
        
    }

    create_div_login() {
        const div = document.createElement("div")
        div.setAttribute("id", "div_login")

        const btn_login = document.createElement("button")
        btn_login.setAttribute("type", "button")
        btn_login.innerHTML = "login"
        btn_login.addEventListener("click", this.login)
        div.appendChild(btn_login)

        const btn_logout = document.createElement("button")
        btn_logout.setAttribute("type", "button")
        btn_logout.innerHTML = "logout"
        btn_logout.addEventListener("click", this.logout)
        div.appendChild(btn_logout)

        const p_user_id_name = document.createElement("span")
        p_user_id_name.style.marginLeft = "5px"
        p_user_id_name.innerHTML += "userId: "
        div.appendChild(p_user_id_name)

        const p_user_id_value = document.createElement("span")
        p_user_id_value.setAttribute("id", "p_user_id_value")
        if(localStorage.getItem("user_id") != "") {
            p_user_id_value.innerHTML = localStorage.getItem("user_id")
        } else {
            p_user_id_value.innerHTML = "None"
        }
        div.appendChild(p_user_id_value)

        return div
    }

    login() {
        let user_id = window.prompt("Bitte trage deine User-ID in das Feld.")
        localStorage.setItem("user_id", user_id)
        document.getElementById("p_user_id_value").innerHTML = user_id
    }

    logout() {
        localStorage.setItem("user_id", "")
        document.getElementById("p_user_id_value").innerHTML = "None"
    }
}