export const get_check_connection = function() {
    const url = process.env.UIT_URL + "/get-check-connection"
    send_get_http_request(url).then(responseData => {
        document.getElementById("svg_rectString").setAttributeNS(null, "fill", "green")
        // console.log(responseData)
    }).catch(err => {
        document.getElementById("svg_rectString").setAttributeNS(null, "fill", "red")
        console.log(err)
    })
}

export const post_data = function(data) {
    const url = process.env.UIT_URL + "/onyx-interaction-tracker/post-statement"
    send_post_http_request(url, data)
    console.log(data)
}

const send_get_http_request = function(url) {
    const promise = new Promise((resolve, reject) => {
        const xhr = new XMLHttpRequest()
        xhr.open("GET", url)
        xhr.responseType = "text"
        xhr.onload = function() { //onload -> wenn Response kommt (auch bei "Fail"-Response)
            if(xhr.status >= 400) {
                reject(xhr.response)
            }
            resolve(xhr.response)
        }
        xhr.onerror = function() { //onerror -> wenn gar keine Response kommt
            reject("No response from the server :-(")
        }
        xhr.send()
    })
    return promise
}

const send_post_http_request = function(url, data) {
    const promise = new Promise((resolve, reject) => {
        const xhr = new XMLHttpRequest()
        xhr.open("POST", url)
        xhr.responseType = "text"
        // xhr.setRequestHeader = "Content-Type", "application/json"
        xhr.setRequestHeader = "Content-Type", "text/plain"
        xhr.onload = function() { //onload -> wenn Response kommt
            if(xhr.status >= 400) {
                reject(xhr.response)
            }
            resolve(xhr.response)
        }
        xhr.onerror = function() {
            reject("no answer from the server")
        }
        xhr.send(JSON.stringify(data))
    })
    return promise
}