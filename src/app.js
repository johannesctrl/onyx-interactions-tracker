const {contain_navbar_width, environment_for_qr_codes, minimize_drag_and_drop_width} = require("./anti_onyx")
const { Custom_Header } = require("./custom_header")
const { Task_Collection } = require("./task_collection")
const { get_check_connection } = require("./rest")
const { Div_Las2peer_Connection } = require("./div_las2peer_connection")
const { Div_Login } = require("./div_login")
const { Div_Tasks } = require("./div_tasks")

window.onload = function() {
    main()
}

const main = function () {
    reset_local_storage()
    import_libraries()
    // user_id_prompt_first()

    const task_collection = new Task_Collection()
    task_collection.create_qr_codes()
    task_collection.add_event_listeners()
    task_collection.create_task_elements()
    console.log(task_collection)

    if(process.env.MODE == "development") {
        const custom_header_tasks = new Custom_Header("tasks", "Tasks")
        const div_tasks = new Div_Tasks().create(task_collection)
        custom_header_tasks.add_div(div_tasks)
        const custom_header_las2peer_connection = new Custom_Header("las2peer-connection", "Las2Peer-Connection")
        custom_header_las2peer_connection.add_div(new Div_Las2peer_Connection().create())

    }
    const custom_header_login = new Custom_Header("login", "Login")
    const div_login = new Div_Login().create_div_login()
    custom_header_login.add_div(div_login)
    
    infinite_loop()
}

const infinite_loop = function() {
    contain_navbar_width()
    environment_for_qr_codes()
    minimize_drag_and_drop_width()
    get_check_connection()

    setTimeout(infinite_loop, 3000)
}

const import_libraries = function() {
    // qrjs
    const qrjs_script = document.createElement("script")
    qrjs_script.setAttribute("src", "https://cdn.jsdelivr.net/gh/englishextra/qrjs2@latest/js/qrjs2.min.js")
}

const reset_local_storage = function() {
    localStorage.setItem("timelineInputs", "")
    localStorage.setItem("startTimestamp", Date.now())
}
