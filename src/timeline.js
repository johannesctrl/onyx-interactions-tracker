import { post_data } from "./rest"

export class Timeline {
    constructor(task_collection) {
        this.jsonVersion = "0"
        this.startTimestamp = localStorage.getItem("startTimestamp")
        this.userId = localStorage.getItem("user_id")
        this.fragebogenTitle = task_collection.title
        this.taskTitle = ""
        this.studyName = "onyx-interaction-tracker" //hardcoded

        this.input_counter = 0
    }

    get_input_counter() {
        this.input_counter += 1
        return this.input_counter
    }

    push(timeline_element) {
        timeline_element.inputCounter = String(this.get_input_counter())
        this.push_to_l2p(timeline_element)
        this.save_to_local_storage(timeline_element)
    }
    
    push_to_l2p(timelineElement) {
        // collecting the information in variables
        const userId = String(localStorage.getItem("user_id"))
        const studyName = String(this.studyName)

        const fragebogenTitle = String(this.fragebogenTitle)
        const taskId = String(timelineElement.taskId)
        const taskTitle = String(timelineElement.taskTitle)
        const taskTypeIndex = String(timelineElement.taskTypeIndex)
        const taskElementId = String(timelineElement.taskElementId)

        const inputFieldIndex = String(timelineElement.inputFieldIndex)
        const taskInputValue = String(timelineElement.taskInputValue)
        const inputCounter = String(timelineElement.inputCounter)
        
        const timestamp = String(timelineElement.timestamp)
        const startTimestamp = String(this.startTimestamp)
        const jsonVersion = String(this.jsonVersion)

        // creating an e-element with the data of the variables
        let e = {}
        e.userId = userId
        e.studyName = studyName
        e.fragebogenTitle = fragebogenTitle
        e.taskId = taskId
        e.taskTitle = taskTitle
        e.taskTypeIndex = taskTypeIndex
        e.taskElementId = taskElementId
        e.inputFieldIndex = inputFieldIndex
        e.taskInputValue = taskInputValue
        e.inputCounter = inputCounter
        e.timestamp = timestamp
        e.startTimestamp = startTimestamp
        e.jsonVersion = jsonVersion

        console.log("post to l2p: " + e)
        post_data(e)

    }

    save_to_local_storage(timeline_element) {
        let timeline_inputs_stored = localStorage.getItem("timelineInputs")
        let new_inputs = {
            "inputs": []
        }
        if((timeline_inputs_stored === null || timeline_inputs_stored === "" || timeline_inputs_stored === "null") == false) { // Timeline_Inputs not empty
            const current_inputs = JSON.parse(timeline_inputs_stored).inputs
            for(let i=0, len=current_inputs.length; i<len; i++) {
                new_inputs.inputs.push(current_inputs[i])
            }
        }
        
        timeline_element.jsonVersion = this.jsonVersion
        timeline_element.startTimestamp = this.startTimestamp
        timeline_element.userId = localStorage.getItem("user_id")
        timeline_element.fragebogenTitle = this.fragebogenTitle
        new_inputs.inputs.push(timeline_element)
        console.log(new_inputs)
        
        localStorage.setItem("timelineInputs", JSON.stringify(new_inputs))
    }
}