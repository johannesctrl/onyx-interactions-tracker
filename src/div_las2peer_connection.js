export class Div_Las2peer_Connection {
    constructor() {

    }

    create() {
        const div_las2peer_connection = document.createElement("div")
        div_las2peer_connection.setAttribute("class", "connectionL2P")
        div_las2peer_connection.innerHTML += `
            <p id="p_connectionL2P">
            <svg xmlns="http://www.w3.org/2000/svg" width="15" height="15"><rect id="svg_rectString" width="15" height="15" fill="black" /></svg>
            Connection to L2P-Service 
            </p>
        `
        return div_las2peer_connection
    }
}