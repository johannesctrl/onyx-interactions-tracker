export class Debugging_Header {
    constructor(task_collection) {
        this.task_collection = task_collection
        this.div_debugging_header = this.create_debugging_header()
        this.add_connection_l2p()
        this.add_tasks(task_collection.task_list)
    }

    create_debugging_header() {
        const header = document.getElementsByTagName("header")[0]
        const div_debugging_header = document.createElement("div")
        div_debugging_header.style.borderStyle = "solid"
        const color = "#74caed"
        div_debugging_header.style.borderColor = color
        div_debugging_header.id = "debugging-header"
        header.appendChild(div_debugging_header)
        div_debugging_header.style.backgroundColor = "#87CEEB"

        // create and append title
        const div_title = document.createElement("div")
        div_title.style.backgroundColor = color
        div_debugging_header.appendChild(div_title)
        div_title.id = "debugging-header-title"
        
        const p_title = document.createElement("p")
        p_title.style.fontWeight = "bold"
        p_title.innerHTML += "Debugging Header"
        div_title.appendChild(p_title)
        
        return div_debugging_header
    }

    add_connection_l2p() {
        this.div_debugging_header.innerHTML += `
            <div class="connectionL2P">
                <p id="p_connectionL2P">
                <svg xmlns="http://www.w3.org/2000/svg" width="15" height="15"><rect id="svg_rectString" width="15" height="15" fill="black" /></svg>
                Connection to L2P-Service
                </p>
            <div>
        `
    }

    add_tasks(task_list) {
        // title
        const title = "Tasks"
        const p_title = document.createElement("p")
        p_title.innerHTML += title
        this.div_debugging_header.appendChild(p_title)

        // Table
        const table = document.createElement("table")

        // Header
        const thead = table.createTHead()
        const row = thead.insertRow()
        let titles = ["number", "title", "id", "type_index", "task_elements"]
        for(let i=0, len=titles.length; i<len; i++) {
            let th = document.createElement("th")
            let text = document.createTextNode(titles[i])

            th.appendChild(text)
            row.appendChild(th)
        }
        
        // Data
        for(let i=0, len=task_list.length; i<len; i++) {

            const data = [
                task_list[i].number, task_list[i].title, task_list[i].id,
                task_list[i].type_index, task_list[i].task_element_list.length
            ]

            let row = table.insertRow()
            for(let j=0, len=data.length; j<len; j++) {
                let cell = row.insertCell()
                let text = document.createTextNode(data[j])
                cell.append(text)
            }
        }
        // CSS
        table.style.border = "1px solid black"
        table.style.width = "100%"

        this.div_debugging_header.appendChild(table)
        
    }

}