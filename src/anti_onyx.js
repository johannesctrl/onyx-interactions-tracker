export const contain_navbar_width = function() {
    try {
        const element_navContainer = document.getElementsByTagName("nav")
        if(element_navContainer != undefined) {
            element_navContainer[0].style.width = "200px"
        }
    }
    catch(err) {
        console.log(err)
    }
}

export const environment_for_qr_codes = function() {
    var divContent = document.getElementsByClassName("content")[0]
    divContent.style.height = "560px"
    divContent.style.overflowY = "auto"
    divContent.style.marginLeft = "0px"
    var divHeaderContainer = document.getElementsByClassName("header-container")[0]
    divHeaderContainer.style.position = "relative"
    divHeaderContainer.style.left = "0px"
}

export const minimize_drag_and_drop_width = function() {
    const ims = document.getElementsByClassName("interaction-match")
    for(let i=0, len=ims.length; i<len; i++) {
        ims[i].style.widht = "auto"
    }
}

