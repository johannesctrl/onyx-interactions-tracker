const qrjs2 = require("./qrjs2") // not necessary
import $ from "jquery"
import { Task_Element_Numbering_Scheme } from "./task_element_numbering"
import { Hottext } from "./hottext"
import { Radio } from "./radio"
import { Task_Element } from "./task_element"
import { Textarea } from "./textarea"
import { Textfield } from "./textfield"
import { Timeline_Element } from "./timeline_element"
import { Tagged_Area } from "./tagged_area"

export class Task {
    constructor(div_item_container, number, task_collection) {
        //html elements
        this.div_item_container = div_item_container
        this.div_interaction_containers = this.get_div_interaction_containers()
        
        //extractions
        this.id = this.extract_id()
        this.title = this.extract_title()
        this.type_index, this.type_name, this.task_type_toggle = this.extract_task_type()
        this.interaction_element = this.extract_interaction_element()
        
        this.task_collection = task_collection
        this.number = number
        this.task_element_list = []

        this.task_element_numbering_scheme = new Task_Element_Numbering_Scheme()
    }
    get_div_interaction_containers() {
        return this.div_item_container.getElementsByClassName("interaction-container")
    }
    extract_id() {
        const a_array = this.div_item_container.getElementsByTagName("a")
        const id = a_array[0].getAttribute("name")
        if(id.substring(0,2) == "id") {
            return id
        }
    }
    extract_title() {
        const div_content_header = this.div_item_container.getElementsByClassName("content-header")
        const h2 = div_content_header[0].getElementsByTagName("h2")
        this.title = h2[0].textContent

        return h2[0].textContent
    }
    extract_task_type() {
        /**
         * This method tries to guess which type of task it is by searching for elementes in
         * element_interaction_container with certain class names "interaction-choice".
         */

        const task_types_list = [
            ["0", "Auswahlaufgabe", "interaction-choice", "0"],
            ["1", "Lückentextaufgabe | Nummerische Eingabe | Berechnung | Formelvergleich | Regulärer Ausdruck",
            "interaction-textentry", "1"],
            ["2", "Freitextaufgabe", "interaction-extendedtext", "0"],
            ["3", "Einfache Zuordnung Drag and Drop", "interaction-match", "0"],
            ["4", "Mehrfache Zuordnung (Matrix)", "interaction-matrix", "0"],
            ["5", "Grafische Zuordnung", "ia_graphic_gap_match outer", "0"],
            ["6", "Reihenfolge Aufgabe", "interaction-match order", "0"],
            ["7", "Hotspotaufgabe", "ia_hotspot", "0"],
            ["8", "Fehlertextaufgabe", "hottextInteraction", "1"],
            ["9", "Textboxaufgabe", "dropdown inline-input", "1"],
            ["10", "Uploadaufgabe", "uploadwrapper", "0"],
            ["11", "Programmieraufgabe", "ace_gutter", "0"],
        ]
        
        for (let i=0, len=task_types_list.length; i<len; i++) {
            
            // values of general_task_types into variables
            const task_type_index = parseInt(task_types_list[i][0])
            const task_type_name = task_types_list[i][1]
            const task_type_class_name = task_types_list[i][2]
            const task_type_toggle = task_types_list[i][3]

            const match = this.div_interaction_containers[0].getElementsByClassName(task_type_class_name)
            
            if(match.length > 0) {
                this.type_index = task_type_index
                this.type_name = task_type_name
                this.type_toggle = task_type_toggle
            }
        }
    }
    create_qr_codes() {
        new Tagged_Area(this.div_item_container, this.number, this.id)
    }
    extract_interaction_element() {
        if(this.type_index == "0") { //Auswahlaufgabe
            console.log("Auswahlaufgabe")
            const radio_list = []
            const input_element_array = this.div_interaction_containers[0].getElementsByTagName("input")
            for(let i=0, len=input_element_array.length; i<len; i++) {
                const input_element = input_element_array[i]
                const element_name = input_element.getAttribute("name")
                const radio_value = input_element.getAttribute("value")
                const radio = new Radio(input_element, radio_value, element_name)
                radio_list.push(radio) // strukturiert nach radio
            }
            return radio_list
        }
        if(this.type_index == "1") { // Lückentext
            console.log("Lückentextaufgabe")
            const textfield_list = []
            for(let i=0, len=this.div_interaction_containers.length; i<len; i++) {
                const element = this.div_interaction_containers[i].getElementsByTagName("input")[0]
                const number = i
                const textfield = new Textfield(element, number)
                textfield_list.push(textfield)
            }
            return textfield_list
        }
        if(this.type_index == "2") { // Freitextaufgabe
            console.log("Freitextaufgabe")
            const element_input = this.div_interaction_containers[0].getElementsByTagName("textarea")
            const textarea = new Textarea(element_input)
            return textarea
        }
        if(this.type_index == "4") { // Matrix
            console.log("Matrixaufgabe")
            const matrix_row_array = this.div_interaction_containers[0].getElementsByClassName("matrix-row")
            let matrix_radios = new Array(matrix_row_array.length)
            for(let i=0, len=matrix_row_array.length; i<len; i++) {
                //console.log(i, "matrix-row")
                const matrix_cell_array = matrix_row_array[i].getElementsByClassName("matrix-cell")
                matrix_radios[i] = new Array(matrix_cell_array.length) 
                for(let j=0, len=matrix_cell_array.length; j<len; j++) {
                    //console.log(j, "matrix-cell")
                    const element_input_array = matrix_cell_array[j].getElementsByTagName("input")
                    const element = element_input_array[0]
                    const radio_value = element.getAttribute("value")
                    const event_name = element.getAttribute("name")
                    const radio = new Radio(element, radio_value, event_name)
                    matrix_radios[i][j] = radio
                }
            }
            return matrix_radios
        }
        if(this.type_index == "8") {
            console.log("Fehlertextaufgabe")
            const hottexts = []
            const element_hottexts = this.div_interaction_containers[0].getElementsByClassName("hottext")
            for(let i=0, len=element_hottexts.length; i<len; i++) {
                const element_input = element_hottexts[i]
                const number = i
                const hottext = new Hottext(element_input, number)
                hottexts.push(hottext) // in's array speichern
            }
            return hottexts
        }
    }
    add_event_listeners() {
        const tl_task_id = this.id
        const tl_task_title = this.title
        const tl_type_name = this.type_name
        const tl_type_index = this.type_index

        const timeline = this.task_collection.timeline

        if(this.type_index == "0") { // Auswahlaufgabe
            const input_name_tag = this.interaction_element[0].element.getAttribute("name")
            const radio_list = this.interaction_element

            $("input[name=\"" + input_name_tag + "\"]").change(function() {
                const check_number = parseInt(this.value.substring(5, this.value.length))
                const check_number_first_radio_value = parseInt(radio_list[0].radio_value.substring(5, this.value.length))
                const number = check_number - check_number_first_radio_value
                const radio = radio_list[number]
                const timestamp = Date.now()

                const tl_number = number
                const tl_timestamp = timestamp
                const tl_inputValue = radio.radio_value //eher unwichtig
                const tl_elementId = crawl_element_id(this)
                const timeline_element = new Timeline_Element(
                    tl_task_id, tl_task_title, tl_elementId,
                    tl_type_index, tl_type_name, tl_number, tl_timestamp,
                    tl_inputValue
                    )
                timeline.push(timeline_element)
            })
        }

        if(this.type_index == "1") { // Lückentext
            const input_list = this.div_item_container.getElementsByTagName("input")

            for(let i=0, len = input_list.length; i<len; i++) {
                const input = input_list[i]
                input.addEventListener("input", function() {

                    const tl_number = i
                    const tl_timestamp = Date.now()
                    const tl_input_value = input.value
                    const tl_element_id = crawl_element_id(this)
                    const timeline_element = new Timeline_Element(
                        tl_task_id, tl_task_title, tl_element_id,
                        tl_type_index, tl_type_name, tl_number, tl_timestamp,
                        tl_input_value)
                    timeline.push(timeline_element)
                }, true)
            }
        }

        if(this.type_index == "2") { // Freitextaufgabe
            const input = this.div_item_container.getElementsByTagName("textarea")[0]
            input.addEventListener("input", function() {
                const current_value = input.value
                const timestamp = Date.now()

                const tl_number = 0
                const tl_timestamp = timestamp
                const tl_inputValue = current_value
                const tl_elementId = crawl_element_id(this)
                const timeline_element = new Timeline_Element(
                    tl_task_id, tl_task_title, tl_elementId,
                    tl_type_index, tl_type_name, tl_number, tl_timestamp,
                    tl_inputValue)
                timeline.push(timeline_element)
            }, true)
        }

        if(this.type_index == "4") { // Matrix
            const radios_matrix = this.interaction_element
            for(let i=0, len=radios_matrix.length; i<len; i++) {
                for(let j=0, len=radios_matrix[i].length; j<len; j++) {
                    const id = radios_matrix[i][j].element.getAttribute("id")
                    $("input[id=\"" + id + "\"]").change(function() {
                        let self = this
                        console.log("value = " + self.value)
                        const check_number = parseInt(self.value.substring(5, self.value.length))
                        const check_number_first_radio_value = parseInt(radios_matrix[0][0].radio_value.substring(5, self.value.length))
                        const number = check_number - check_number_first_radio_value
                        
                        const row_number = number % radios_matrix[i].length
                        const col_number = Math.floor(number / radios_matrix[i].length)

                        const tl_elementId = crawl_element_id(this)
                        const tl_number = String(row_number) + " " + String(col_number)
                        const tl_timestamp = Date.now()
                        const tl_inputValue = self.value
                        const timeline_element = new Timeline_Element(tl_task_id, tl_task_title, tl_elementId, tl_type_index, tl_type_name, tl_number, tl_timestamp, tl_inputValue) 
                        timeline.push(timeline_element)

                    })
                }
            }
        }

        if(this.type_index == "8") { // Fehlertext
            console.log("Fehlertext")
            const inputs = this.div_interaction_containers[0].getElementsByTagName("input")
            const hottexts = this.interaction_element
            for(let i=0, len=hottexts.length; i<len; i++) {
                const input = hottexts[i].element
                input.addEventListener("click", function() {
                    console.log("Fehlertext-Input geklickt")
                    const input_number = i
                    const current_value = inputs[i].value
                    const timestamp = Date.now()

                    // Ordered by global timeline
                    const tl_number = input_number
                    const tl_timestamp = timestamp
                    const tl_inputValue = current_value
                    const tl_elementId = crawl_element_id(this)
                    const timelineElement = new Timeline_Element(
                        tl_task_id, tl_task_title, tl_elementId,
                        tl_type_index, tl_type_name, tl_number, tl_timestamp,
                        tl_inputValue)
                    timeline.push(timelineElement)
                })
            }            
        }
    }
    create_task_elements() {
        this.create_explaination_and_interaction_section()
        this.create_image_elements()
    }
    create_image_elements() {
        const figures = this.div_item_container.getElementsByTagName("img")
        for(let i=0, len=figures.length; i<len; i++) {
            this.task_element_list.push(new Task_Element(figures[i], "figure", this))
        }
        
    }
    create_explaination_and_interaction_section() {
        if(this.type_index != 1) { // Lückentexte Tasks "do not have" explaination sections
            let item_content = this.div_item_container.getElementsByClassName("item-content")[0]
            
            // create explaination section
            let explaination_section = document.createElement("div")
            this.div_item_container.appendChild(explaination_section)
            explaination_section.append(...item_content.childNodes)
            item_content.append(explaination_section)
            if(explaination_section.childElementCount > 0) { //Falls eine explaination section existiert
                console.log(this.title + "explaination-section exists")
                this.task_element_list.push(new Task_Element(explaination_section, "explaination-section", this))
            } else {
                explaination_section.remove()
            }

            // create interaction section
            let interaction_section = document.createElement("div")
            this.div_item_container.appendChild(interaction_section)
            interaction_section.appendChild(this.div_interaction_containers[0])
            item_content.append(interaction_section)
            this.task_element_list.push(new Task_Element(interaction_section, "interaction-section", this))
        }

        if(this.type_index == 1) { // Textfield
            for(let i=0, len=this.interaction_element.length; i<len; i++) {
                console.log(i)
                const textfield = this.interaction_element[i]
                const task_element = new Task_Element(textfield.element, "textfield", this)
                this.task_element_list.push(task_element)
            }
        }
        if(this.type_index == 8) { // Hottext
            for(let i=0, len=this.interaction_element.length; i<len; i++) {
                console.log(i)
                const hottext = this.interaction_element[i]
                const task_element = new Task_Element(hottext.element, "textfield", this)
                this.task_element_list.push(task_element)
            }
        }
            
    }

}

export const extract_tasks = function() {
    const task_list = []
    const div_item_container = document.getElementsByClassName("item-container")
    for (let i = 0, len = div_item_container.length; i < len; i++) {
        const task = new Task(div_item_container[i], i)
        task_list.push(task)
    }
    return task_list
}


const crawl_element_id = function(element) {
    /*
     * This Method is used on an input-element and climbs up the hierarchy tree
     * to find its elementId which it was tagged with.
     */
    while(element.parentElement) { //solange element ein Parent hat
        // console.log(element)
        if(element.getAttribute("task-element-id") != null) {
            const marker_id = element.getAttribute("task-element-id")
            return marker_id
        } else {
            element = element.parentNode
        }
    }
    return 999
}