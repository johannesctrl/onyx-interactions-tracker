var $ = require( "jquery" )

export const inline_marking_element = function(element, marker_id) {
    element.setAttribute("marker-id", marker_id)
	element.style.marginLeft = "5px"
	element.style.marginRight = "5px"
    const arucoMarkerLeft = return_aruco_marker_element(marker_id)
    const arucoMarkerRight = return_aruco_marker_element(marker_id)
    element.before(arucoMarkerLeft)
    element.after(arucoMarkerRight)
}

export const adaptive_marking_element = function(task_number, element, marker_id) {
    /**
     * In dieser Methode werden für ein zu markierendes Element links und rechts davon div-Container erzeugt, um darin die Marker einzusetzen.
     */

    // create element hierarchy
    const tagged_area = document.createElement("div")
    tagged_area.className = "tagged-area"
    tagged_area.setAttribute("marker-id", marker_id)
    const div_left = document.createElement("div")
    div_left.className = "div-left"
    const div_code_left = document.createElement("div")
    div_code_left.setAttribute("id", "div-code-left_" + task_number + "_" + marker_id)
    const div_middle = document.createElement("div")
    div_middle.className = "div-middle"
    const div_right = document.createElement("div")
    div_right.className = "div-right"
    const div_code_right = document.createElement("div")
    div_code_right.setAttribute("id", "div-code-right_" + task_number + "_" + marker_id)
    element.before(tagged_area)
    div_middle.appendChild(element)
    tagged_area.appendChild(div_left)
    tagged_area.appendChild(div_middle)
    tagged_area.appendChild(div_right)
    div_left.appendChild(div_code_left)
    div_right.appendChild(div_code_right)
    create_aruco_marker_element(div_code_left, marker_id)
    create_aruco_marker_element(div_code_right, marker_id)
    
    // CSS
    tagged_area.style.display = "flex"
    div_right.style.display = "flex"
    div_code_left.style.position = "sticky"
    div_code_left.style.top = "10px"
    div_code_left.style.margin = "10px"
    div_code_right.style.position = "sticky"
    div_code_right.style.marginTop = "auto"
    div_code_right.style.marginLeft = "10px"
    div_code_right.style.marginBottom = "10px"
    div_code_right.style.marginRight = "10px"
	div_code_right.style.paddingTop = "10px" // padding weil marginTop auf "auto" muss
    div_code_right.style.bottom = "10px"
    div_middle.style.padding = "10px"
    div_middle.style.overflowX = "auto" // in ArUCo
}

function generate_marker_svg(width, height, bits) {
	var svg = $('<svg/>').attr({
		viewBox: '0 0 ' + (width + 2) + ' ' + (height + 2),
		xmlns: 'http://www.w3.org/2000/svg',
		'shape-rendering': 'crispEdges' // disable anti-aliasing to avoid little gaps between rects
	});
	$('<rect/>').attr({ // Background rect
		x: 0,
		y: 0,
		width: width + 2,
		height: height + 2,
		fill: 'black'
	}).appendTo(svg);
	for (var i = 0; i < height; i++) { // "Pixels"
		for (var j = 0; j < width; j++) {
			var color = bits[i * height + j] ? 'white' : 'black';
			var pixel = $('<rect/>').attr({
				width: 1,
				height: 1,
				x: j + 1,
				y: i + 1,
				fill: color
			});
			pixel.appendTo(svg)
		}
	}
	return svg
}

function generate_aruco_marker(width, height, dict_name, id) {
    
    const dict = require("./aruco_dict.json")
	var bytes = dict[dict_name][id];
	var bits = [];
	var bitsCount = width *  height;

	// Parse marker's bytes
	for (var byte of bytes) {
		var start = bitsCount - bits.length;
		for (var i = Math.min(7, start - 1); i >= 0; i--) {
			bits.push((byte >> i) & 1);
		}
	}
	return generate_marker_svg(width, height, bits);
}

function create_aruco_marker_element(div_marker, marker_id) {
	// define marker attributes
	var size = 8
	var dict_name = "4x4_1000"
	var width = 4
	var height = 4

	var svg = generate_aruco_marker(width, height, dict_name, marker_id, size);
	svg.attr({
		width: size + 'mm',
		height: size + 'mm'
	});

	const id_name = "#" + div_marker.id
	$(id_name).html(svg[0].outerHTML) //append created svg-Marker to marker-container
}

function return_aruco_marker_element(markerId) {
	// define marker attributes
	var size = 8
	var dictName = "4x4_1000"
	var width = 4
	var height = 4

	var svg = generate_aruco_marker(width, height, dictName, markerId, size);
	svg.attr({
		width: size + 'mm',
		height: size + 'mm'
	});

    const body = document.body

    const aruco_marker_placeholder = document.createElement("div")
    aruco_marker_placeholder.setAttribute("id", "aruco-marker-placeholder")

    body.appendChild(aruco_marker_placeholder)

	$("#aruco-marker-placeholder").html(svg[0].outerHTML) //append created svg-Marker to marker-container
    const svgElement = document.getElementById(aruco_marker_placeholder.id).firstElementChild
    return svgElement
}
