export class Timeline_Element {
    constructor(id, title, element_id, type_index, type_name, input_field_index, timestamp, input_value) {
        this.taskId = String(id)
        this.taskTitle = String(title)
        this.taskElementId = String(element_id)
        this.taskTypeIndex = String(type_index)
        // this.typeName = typeName
        this.inputFieldIndex = String(input_field_index)
        this.timestamp = String(timestamp)
        this.taskInputValue = String(input_value)
    }
}