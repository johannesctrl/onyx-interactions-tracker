export class Custom_Header {
    constructor(name, title) {
        this.div_custom_header = this.create_header(name, title)
    }

    create_header(name, title) {
        const header = document.getElementsByTagName("header")[0]
        const div_header = document.createElement("div")
        // div_header.style.borderStyle = "solid"
        // div_header.style.borderColor = "#cfcfcf"
        div_header.id = "custom-header-" + name
        header.appendChild(div_header)

        // create and append title
        const div_title = document.createElement("div")
        div_title.style.color = "#ffffff"
        div_title.style.backgroundColor = "#343a40"
        div_title.style.paddingLeft = "5px"
        div_title.style.paddingRight = "5px"
        div_header.appendChild(div_title)
        div_title.id = "custom-header-" + name + "-title"
        
        const p_title = document.createElement("p")
        p_title.style.fontWeight = "bold"
        p_title.innerHTML += title
        div_title.appendChild(p_title)
        
        return div_header
    }
    add_div(div) {
        div.style.padding = "5px"
        div.style.backgroundColor = "#dcdcdc"
        this.div_custom_header.appendChild(div)
    }
}