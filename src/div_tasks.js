export class Div_Tasks {
    constructor() {

    }
    create(task_collection) {
        const task_list = task_collection.task_list
        const div_debugging_header = document.createElement("div")

        // Table
        const table = document.createElement("table")

        // Header
        const thead = table.createTHead()
        const row = thead.insertRow()
        let titles = ["number", "title", "id", "type_index", "task_elements"]
        for(let i=0, len=titles.length; i<len; i++) {
            let th = document.createElement("th")
            let text = document.createTextNode(titles[i])

            th.appendChild(text)
            row.appendChild(th)
        }
        
        // Data
        for(let i=0, len=task_list.length; i<len; i++) {

            const data = [
                task_list[i].number, task_list[i].title, task_list[i].id,
                task_list[i].type_index, task_list[i].task_element_list.length
            ]

            let row = table.insertRow()
            for(let j=0, len=data.length; j<len; j++) {
                let cell = row.insertCell()
                let text = document.createTextNode(data[j])
                cell.append(text)
            }
        }
        // CSS
        table.style.border = "1px solid black"
        table.style.width = "100%"

        div_debugging_header.appendChild(table)
        
        return div_debugging_header
    }
}