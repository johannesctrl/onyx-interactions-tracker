import { Timeline } from "./timeline"
import { Task } from "./task"

export class Task_Collection {
    constructor() {
        this.title = this.get_task_collection_title()
        this.task_list = this.extract_tasks()
        this.timeline = new Timeline(this)
    }

    create_task_elements() {
        for (let i=0, len=this.task_list.length; i<len; i++) {
            this.task_list[i].create_task_elements()
        }
    }

    create_qr_codes() {
        for (let i=0, len=this.task_list.length; i<len; i++) {
            this.task_list[i].create_qr_codes()
        }
    }

    add_event_listeners() {
        for (let i=0, len=this.task_list.length; i<len; i++) {
            this.task_list[i].add_event_listeners()
        }
    }

    extract_tasks() {
        const task_list = []
        const div_item_container = document.getElementsByClassName("item-container")
        for (let i = 0, len = div_item_container.length; i < len; i++) {
            const task = new Task(div_item_container[i], i, this)
            task_list.push(task)
        }
        return task_list
    }

    get_task_collection_title = function() {
        try {
            const element_navbar = document.getElementsByClassName("nav-header")[0]
            const title = element_navbar.getElementsByTagName("h1")[0].innerHTML
            return title
        }
        catch(err) {
            console.log("No title of task collection exists.")
            return "None"
        }
    }
}

