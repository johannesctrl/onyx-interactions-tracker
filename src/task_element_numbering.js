
export class Task_Element_Numbering_Scheme {
    constructor() {
        // define all Aruco_Numbering_Group there are
        this.explaination_section = new Task_Element_Numbering_Group(0, 1, this)
        this.interaction_section = new Task_Element_Numbering_Group(1, 1, this)
        this.figure = new Task_Element_Numbering_Group(10, 19, this)
        this.textfield = new Task_Element_Numbering_Group(20, 80, this)

        this.number_array = [] // saves all numbers which were used to check if the current number is valid
    }
    check_if_number_is_valid(number) {
        if(number > 999) {
            console.log("Err: Invalid Number is bigger than 999.")
            return false
        } else if (typeof this.number_array[number] !== 'undefined') {
            console.log("Err: Number has already been taken.")
            return false
        } else {
            return true
        }
    }
}

class Task_Element_Numbering_Group {
    // This class is used in the Aruco_Numbering_Scheme-Class for every group that needs a numbering system
    constructor(starting_number, slots, scheme) {
        this.starting_number = starting_number
        this.slots = slots // size of the number range the element is allowed to have, eg. explainationSection has 1
        this.counter = 0
        this.scheme = scheme // saves scheme-obj into variable for use of checkIfNumberIsValid()-function
    }
    get_number() {
        // supplies a number for the aruco marker
        if(this.slots == 0) {
            console.log("No slots left for this type of numbering.")
            return 999
        }
        
        const marker_id = this.starting_number + this.counter
        this.counter += 1
        this.slots -= 1

        if(this.scheme.check_if_number_is_valid(marker_id)) {
            return marker_id
        } else {
            throw "Err: The following number is invalid:" + String(marker_id)
        }
    }
}