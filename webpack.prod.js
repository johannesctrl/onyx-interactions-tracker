const path = require("path")
const common = require("./webpack.common")
const { merge } = require("webpack-merge")
const { EnvironmentPlugin } = require("webpack")

module.exports = merge(common, {
    mode: "production",
    output: {
        path: path.resolve(__dirname, "dist"),
        filename: "[name].prod.bundle.js",
        clean: true,
    }, 
    plugins: [
        new EnvironmentPlugin({
          UIT_URL: "https://las2peer.tech4comp.dbis.rwth-aachen.de/las2peer-onyx-interaction-tracker",
          MODE: "production",
        }),
      ],
})