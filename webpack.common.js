const path = require("path")

module.exports = {
    entry: {
        main: path.resolve(__dirname, "src/app.js"),
    },
    output: {
        path: path.resolve(__dirname, "dist"),
        filename: "[name].bundle.js",
        clean: false,
    },
    //loaders
    //plugins
    resolve: {
      modules: [path.resolve(__dirname, 'src'), 'node_modules'],
    },
}