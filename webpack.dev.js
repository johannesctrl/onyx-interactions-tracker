const path = require("path")
const common = require("./webpack.common")
const { merge } = require("webpack-merge")
const { EnvironmentPlugin } = require("webpack")

module.exports = merge(common, {
    mode: "development",
    output: {
        path: path.resolve(__dirname, "dist"),
        filename: "[name].dev.bundle.js",
        clean: true,
    },
    plugins: [
        new EnvironmentPlugin({
          UIT_URL: "https://ki-gateway.tech4comp.dbis.rwth-aachen.de/lrs-requests-server",
          MODE: "development",
        }),
      ],
})